---
layout: post
title:  "Dev Update - Critical Hit!"
date:   2015-03-23 18:00:00
category: Devlog
tags: [Heretic Sun, combat, animation, flavor]
---

Felt like working on some more visual stuff today.

First off, I've had the logic for critical hits implemented for some time, but they weren't very flashy. I wanted to fix that.

![crit](http://zippy.gfycat.com/ResponsibleDarkKusimanse.gif)

Not a bad start, in my opinion.

The jerky camera has been bothering me for a while now as well.

![smooth](http://giant.gfycat.com/BlissfulEmotionalHyena.gif)

The camera is much less disorienting now, especially when the camera moves long distances.

---
layout: post
title:  "Dev Update - Animating Combat"
date:   2015-03-17 16:00:00
category: Devlog
tags: [Heretic Sun, combat, animation]
---

Now that I have a lot of the combat logic in place, I decided to start animating combat.
Even a little animation goes a long way.

There were a few things to consider.

One can minimize a lot of the work by simply flipping sprites. However, since this game is isometric,
I need to have different animations depending on whether the character is facing the "camera" or not.
Also, if the character is holding something, flipping the sprites would cause the object to change hands,
which is immersion-breaking (for me, at least).

I wanted the ability to have both left- and right-handed characters, and the ability to visually represent different weapon types.
So really, animating combat involves three separate kinds of animation: characters, weapons, and effects (in this case, the slash trail).

Separating the character from the weapons will (hopefully) make it easier to re-use animations. The animation
code contains information about weapon offsets, so it shouldn't be too hard to re-position weapon animations for
different classes.

Here's a demonstration.

First, we have a battle with a left-handed attacker, using a sword, and a right-handed defender, using a dagger.
![left hands](http://zippy.gfycat.com/SpitefulObviousKronosaurus.gif)

Now, we make the attacker right-handed, and the defender left-handed.
![right hands](http://zippy.gfycat.com/AdeptWindyAfricanhornbill.gif)

Fun! The animations still need a few tweaks, but I'm pretty satisfied with this starting point.

---
layout: post
title:  "Dev Update - Particle Man"
date:   2015-04-01 18:45:00
category: Devlog
tags: [Heretic Sun, flavor, animation, weather]
---

This is not an April Fool's post, I promise!

Last week I started tackling two separate features: the weather system and magic attacks. Both of these require the use of particle effects.

The weather is composed of various components: precipitation type, precipitation intensity, temperature, and wind speed.
These components change from turn to turn based on a probability model given to the weather system.
Each of the components has various effects on gameplay. For example, when it is windy, archers have a penalty to shot accuracy.
The following GIF took displays medium-intensity rain with high wind.

![particles](http://fat.gfycat.com/GloriousEsteemedHedgehog.gif)

Particle effects also add some spice to magic attacks.

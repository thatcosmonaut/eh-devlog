---
layout: post
title:  "Dev Update - Sun Arrow"
date:   2015-03-20 20:00:00
category: Devlog
tags: [Heretic Sun, combat, animation, math, flavor]
---

I've been working on an important feature: ranged combat animations!

One should never use math which is more complicated than necessary. I have had to learn this lesson repeatedly.

All the curvey animations in this game, like jumping and projectiles, are implemented using [Bézier curves](http://en.wikipedia.org/wiki/B%C3%A9zier_curve), which have a complicated-sounding name
but are very simple to use.

Originally I tried to set up curves using physics equations. It was really complicated, took hours, and the result was that
everything looked like shit and I had no idea what was going on.

A quadratic Bézier curve, on the other hand, uses a simple algebraic equation.

![jump](http://zippy.gfycat.com/UntimelyDeliciousKookaburra.gif)

Much better.

I repeated this complicated-math mistake again today. Arrows need to adjust their angle during flight in order to look believable. I thought to myself, "What if I find the angle formed by the tangent of the Bézier curve and the x-axis?"
After many hours of confusing, broken equations and wildly-rotating arrows, I decided to simply set the arrow angles manually depending on the relative x and y positions of the firer and target.

![arrowed](http://zippy.gfycat.com/EthicalPhysicalHedgehog.gif)

Much better.

As also evidenced by the above image, palette swaps have been implemented, which should hopefully lend some variety to the characters and weapons.

Characters can now block if they have equipped an item which can be used to block. Otherwise they will evade, as before.
The difference between blocking and evading is purely cosmetic.

![bouncey](http://zippy.gfycat.com/PoliteScarceAcornbarnacle.gif)

The characters in battle-related text now bounce individually as well. Festive!
